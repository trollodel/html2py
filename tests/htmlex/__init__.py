#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Parse and save samples."""

import os
import lxml.etree
__all__ = ("example_files", "example_trees")

example_files = []
example_trees = []


def init():
    global example_files, example_trees
    """Init samples."""
    dir = os.path.dirname(os.path.abspath(__file__))
    for file in os.listdir(dir):
        if file.endswith(".html"):
            example_files.append(open(os.path.join(dir, file)))
    for file in example_files:
        example_trees.append(lxml.etree.HTML(file.read()))
        file.seek(0)


if __name__ != '__main__':
    init()
