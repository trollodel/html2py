#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Html2py test suite.

DO NOT USE THIS CODE FOR PRODUCTIONS.
Due to exec calls in test cases, the code in this module is very insecure.
"""

from . import parser

__all__ = ("parser")
