#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Test parsers.

TODO: add xml random generator.
"""

import io

from . import htmlex  # coming soon
import html2py

import lxml.etree
import yattag

import xmldiff.diff
import xmldiff.main
from hypothesis import given
from hypothesis.strategies import from_regex, composite, sampled_from, integers

__all__ = ("test_xml_parser_one_tag", "test_html_parser")  # "test_html_parser"


def generate_word(draw):
    return draw(from_regex(r"[a-zA-Z]{1,10}", fullmatch=True))


@composite
def create_tag(draw):
    """Create line tag."""
    return "<{0}>{1}{1}</{0}>".format(generate_word(draw), generate_word(draw))


@given(create_tag())
def test_xml_parser_one_tag(intag):
    """Test XML parser with only one tag."""
    tree = lxml.etree.XML(intag)
    buf = io.StringIO()
    parser = html2py.parser.XMLParser()
    yf = html2py.yattag.YattagOutput(buf, parser)
    yf.process_tree(tree)
    yf.apply()
    buf.seek(0)
    doc, tag, text, line = yattag.Doc().ttl()
    asis = doc.asis
    stag = doc.stag
    exec(buf.read())
    tree2 = lxml.etree.XML(doc.getvalue())
    assert not xmldiff.main.diff_trees(tree, tree2)


@given(sampled_from(htmlex.example_trees))
def test_html_parser(tree):
    """Test HTML parser with samples."""
    buf = io.StringIO()
    parser = html2py.parser.HTMLParser()
    yf = html2py.yattag.YattagOutput(buf, parser, doctype="<!doctype html>")
    yf.process_tree(tree)
    yf.apply()
    buf.seek(0)
    doc, tag, text, line = yattag.Doc().ttl()
    asis = doc.asis
    stag = doc.stag
    exec(buf.read())
    buf.seek(0)
    res = doc.getvalue()
    tree2 = lxml.etree.HTML(res)
    diff = xmldiff.main.diff_trees(tree, tree2)
    if diff:
        assert all([isinstance(x, xmldiff.diff.UpdateTextAfter) for x in diff])
        return
    assert not diff


@given(sampled_from(htmlex.example_trees), integers(min_value=1, max_value=10))
def test_html_parser_multiple_parsing(tree, repeat):
    """
    Test HTML parser with samples.

    This test repeat parsing and generation many times to check if parsing can loss data.
    """
    for i in range(repeat):
        buf = io.StringIO()
        parser = html2py.parser.HTMLParser()
        yf = html2py.yattag.YattagOutput(buf, parser, doctype="<!doctype html>")
        yf.process_tree(tree)
        yf.apply()
        buf.seek(0)
        doc, tag, text, line = yattag.Doc().ttl()
        asis = doc.asis
        stag = doc.stag
        exec(buf.read())
        buf.seek(0)
        res = doc.getvalue()
        tree2 = lxml.etree.HTML(res)
        diff = xmldiff.main.diff_trees(tree, tree2)
        if diff:
            assert all([isinstance(x, xmldiff.diff.UpdateTextAfter) for x in diff])
            continue
        assert not diff
